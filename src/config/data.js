export default {
    Brief: {
        name: '沈宇',
        position: '前端开发工程师',
        location: '杭州',
        sex: '男',
        age: 26,
        degree: '5年工作经验'
    },
    Contact: [
        { icon: require('@/assets/contact-phone.png'), key: 'phone', value: '13867542620' },
        { icon: require('@/assets/contact-mail.png'), key: 'mail', value: 'shenyu@hotmail.com' }
    ],
    Social: [
        { icon: require('@/assets/gitee.png'), key: 'gitee', value: 'gitee.com/shen-yu' },
        { icon: require('@/assets/social-github.png'), key: 'github', value: 'github.com/shen-yu' }
    ],
    Skill: [
        { icon: require('@/assets/js.png'), key: 'js', value: 'JavaScript' },
        { icon: require('@/assets/skill-vue.png'), key: 'vue', value: 'Vue' },
        { icon: require('@/assets/skill-react.png'), key: 'react', value: 'React' },
        { icon: require('@/assets/css.png'), key: 'css', value: 'css' }
    ],
    Expect: [
        { icon: require('@/assets/money.png'), key: 'vue', value: '20K-25K' }
    ],
    Intent: [
        { icon: require('@/assets/work.png'), key: 'vue', value: '已离职，可随时到岗' }
    ],
    Wechat: [
        { icon: require('@/assets/social-wechat.png'), key: 'wechat', value: require('@/assets/qrcode.png') }
    ],
    AboutMe: `
    web前端，项目经历比较丰富，用Vue开发过多个H5和小程序，也用React和antd开发过多个pc端后台，Vue和React的使用经验都超过4年了，深知前端工程化和代码规范的重要性，在平日开发中自觉遵守ESlint规范，熟知各种常用工具函数的封装，并且能顾全项目整体，有过大型电商前后台从0到1的开发经验。平时喜欢尝试业内最新的技术。
    <br><br>
    我的优势：对各个端都比较有经验，尤其是在移动端，遇到bug能快速定位并给出解决方案。热爱技术，自学能力强，平时能用node和python写一些爬虫，能适应加班，能耐心对待每一个bug，良好的脾气与心态。。
    `,
    Education: [
        { school: '浙江大学城市学院', major: '计算机科学与技术', date: '2013.9-2017.6', description: '' }
    ],
    WorkingExperience: [
        {
            company: '浙江英树生物科技有限公司',
            position: '前端工程师',
            date: '2018.12-至今',
            description: `
                主要负责英树生活H5从0到1的开发，用Vue和vant搭建。参与了英树生活小程序的开发，用mpvue搭建，另外还参与运营后台的开发，用React和Antd搭建。负责的模块主要有商品，订单，售后等。
            `
        },
        {
            company: '新光集团',
            position: '前端工程师',
            date: '2017.9-2018.10',
            description: `
            主要用Vue全家桶开发了一个汽配电商中后台，我在其中负责的是一个面向商家端的后台管理系统，项目是公司和印尼金光保险合作的，网址：tokoonderdil.id，另外还参与开发了四季严选H5端的组件，用到React。由于是跟印尼公司合作的项目，所以经常要出差驻场去，后来就跳到目前这家了。
            `
        },
        {
            company: '杭州千乐互动科技有限公司',
            position: '前端开发',
            date: '2016.6-2017.9',
            description: `
            第一家公司（包括实习），主要产品是一个返利网站——乐豆玩，我在里面主要工作就是用jquery和bootstrap写写页面，后期由于公司面临资金压力，开始尝试外包项目，其中做了一个网站（云雇网）和两个微信小程序（自品优订，名片多），前端部分都由我一个人完成，最后公司倒闭。
            `
        }
    ],
    ProjectExperience: `
        <p><b>移动端：</b>英树生活H5，四季严选H5</p>
        <p><b>微信小程序：</b>英树生活，自品优订，名片多</p>
        <p><b>PC端：</b>英树生活后台，tokoonderdil汽配电商，乐豆玩</p>
    `
}
